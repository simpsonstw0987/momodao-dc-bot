import discord
intents = discord.Intents.all()
intents.typing = False
intents.presences = False
from discord.ext import commands
import json
import requests
import os
import datetime

with open('setting.json', 'r', encoding='utf8') as jfile:
    jdata = json.load(jfile)

bot = commands.Bot(command_prefix= '!', intents=intents)

def is_me():
    def predicate(ctx):
        return ctx.message.author.id == int(jdata['bot_author']) or ctx.message.author.id == int(jdata['server_owner'])
    return commands.check(predicate)

@bot.event
async def on_ready():
  print(">> Bot is online <<")


@bot.command()
async def price(ctx, coin):
    url = jdata["Binance_Price_API"]+coin.upper()+"USDT"
    data = requests.get(url)
    if(data.status_code == requests.codes.ok):
        data = data.json()
        await ctx.send(f'**`{coin.upper()}/USDT` ** 的價格: **`{round(float(data["price"]),3)}`**      `來源：Binance.com`')
    else:
        await ctx.send(f'幣安沒有 `{coin.upper()}` 貨幣')
    now_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    print(f'[{now_time}] {ctx.author} 查詢了 {coin}')

@bot.command()
@is_me()
async def load(ctx, extension):
    bot.load_extension(f'cmds.{extension}')
    await ctx.send(f'Loaded {extension} done')
    now_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    print(f'[{now_time}] {ctx.author} 讀取了 {extension}')

@bot.command()
@is_me()
async def unload(ctx, extension):
    bot.unload_extension(f'cmds.{extension}')
    await ctx.send(f'unLoaded {extension} done')
    now_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    print(f'[{now_time}] {ctx.author} 卸載了 {extension}')

@bot.command()
@is_me()
async def reload(ctx, extension):
    bot.reload_extension(f'cmds.{extension}')
    await ctx.send(f'reLoaded {extension} done')
    now_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    print(f'[{now_time}] {ctx.author} 重新讀取了 {extension}')

for Filename in os.listdir('./cmds'):
    if Filename.endswith('.py'):
         bot.load_extension(f'cmds.{Filename[:-3]}')

if __name__ == "__main__":
    bot.run(jdata['TOKEN'])