import discord
import json
intents = discord.Intents.all()
intents.typing = False
intents.presences = False
from discord.ext import commands

with open('setting.json', 'r', encoding='utf8') as jfile:
    jdata = json.load(jfile)

class Cog_Extension(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
