import discord
import json
import random
import datetime

intents = discord.Intents.all()
intents.typing = False
intents.presences = False
from discord.ext import commands
from core.classes import Cog_Extension

with open('setting.json', 'r', encoding='utf8') as jfile:
    jdata = json.load(jfile)

def is_me():
    def predicate(ctx):
        return ctx.message.author.id == int(jdata['bot_author']) or ctx.message.author.id == int(jdata['server_owner'])
    return commands.check(predicate)

class Main(Cog_Extension):
    
    @commands.command()
    @is_me()
    async def ping(self, ctx):
        await ctx.send(f'{round(self.bot.latency*1000)} 毫秒')
        now_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        print(f'[{now_time}] {ctx.message.author} 使用了 !ping')

    @commands.command()
    async def eat(self, ctx):
        random_eat = random.choice(jdata['吃什麼'])
        await ctx.send(random_eat)
        now_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        print(f'[{now_time}] {ctx.message.author} 使用了 !eat')

def setup(bot):
    bot.add_cog(Main(bot))
