import discord
import json,datetime,asyncio

intents = discord.Intents.all()
intents.typing = False
intents.presences = False
from discord.ext import commands
from core.classes import Cog_Extension

with open('setting.json', 'r', encoding='utf8') as jfile:
    jdata = json.load(jfile)

class Task(Cog_Extension):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        async def time_task():
            await self.bot.wait_until_ready()
            self.channel = self.bot.get_channel(jdata['Hour_Channel'])
            while not self.bot.is_closed():
                now_time = datetime.datetime.now().strftime('%H%M')
                for hour_time in jdata['time']:
                    if now_time == hour_time and self.counter == 0:
                        right_time = int(now_time[0:2])
                        if(right_time >= 13):
                            right_time = right_time - 12
                            time_pic = discord.File(jdata['keyword_pic_path'] + format(right_time) + ".png")
                            await self.channel.send(file = time_pic) 
                        else:
                            time_pic = discord.File(jdata['keyword_pic_path'] + format(right_time) + ".png")
                            await self.channel.send(file = time_pic)                                                   
                        self.counter = 1
                        break     
                    else:
                        self.counter = 0
                if self.counter == 1: 
                    await asyncio.sleep(60)
                else: 
                    await asyncio.sleep(10)        
        self.bg_task = self.bot.loop.create_task(time_task())
        
        

def setup(bot):
    bot.add_cog(Task(bot))