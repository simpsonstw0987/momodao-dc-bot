import json
import discord
import re

from core.classes import Cog_Extension

intents = discord.Intents.all()
intents.typing = False
intents.presences = False
from discord.ext import commands

with open('setting.json', 'r', encoding='utf8') as jfile:
    jdata = json.load(jfile)

class Event(Cog_Extension):

    @commands.Cog.listener()
    async def on_member_join(self,member):
        channel = self.bot.get_channel(int(jdata['Welcome_Channel']))
        welcome_pic = discord.File(jdata['keyword_pic_path'] + "歡迎光臨.png")
        await channel.send(file = welcome_pic)
        await channel.send(f'你好 {member.mention} 點一杯 摸摸熊 半糖少冰大珍珠🧋')

        print(f'{member} join!')

#    @commands.Cog.listener()
#    async def on_member_remove(self, member):
#        channel = self.bot.get_channel(int(jdata['Leave_Channel']))
#        await channel.send(f'{member} leave')
#        print(f'{member} leave!')
  
    @commands.Cog.listener()
    async def on_message(self, msg):
        if msg.author != self.bot.user:
            if re.search ("(?i)momo", msg.content) or "摸摸" in msg.content: # 偵測momo or 摸摸上emoji
                    await msg.add_reaction(jdata['momo_emoji'])
            for en_key in jdata['en_keyword']: # 偵測英文關鍵字上貼圖
                if re.search ("(?i)"+format(en_key), msg.content):
                    keyword = format(en_key)
                    pic = discord.File(jdata['keyword_pic_path'] + keyword.lower() + ".png")
                    await msg.channel.send(file = pic)
                    break
            for key in jdata['關鍵字']: # 偵測中文關鍵字上貼圖
                if key in msg.content:
                    pic = discord.File(jdata['keyword_pic_path'] + format(key) + ".png")
                    await msg.channel.send(file = pic)
                    break

def setup(bot):
    bot.add_cog(Event(bot))
