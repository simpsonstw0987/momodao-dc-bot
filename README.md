# MomoDao-DC-Bot

MomobearDAO discord bot

## VSCode - python

1. 需要用到的模組 Discord.py，安裝方式：pip install -U discord.py

## Getting started

1. 下載後並解壓縮到任何的資料
2. 打開momo\setting.json設定：
- TOKEN: Discord Bot Token
- Bot_author: 機器人擁有者Discord ID* 
- server_owner: 頻道伺服器擁有者Discord ID 
- Welcome_Channel: 歡迎頻道ID** 
- Leave_Channel: 離開頻道ID (備註：此程式碼已經隱藏，這是用來題是誰離開伺服器)
- Hour_Channel: 整點報時頻道ID
- momo_emoji: 關鍵字自動上emoji (如果使用的emoji是在別的群組，機器人也要在那個群組才能跨群組使用)
- keyword_pic_path: 關鍵字圖片檔案資料夾路徑
- *(在DC對使用者右鍵複製ID)   **(在DC對頻道右鍵複製ID)
3. 用命令提示窗輸入 python bot.py
